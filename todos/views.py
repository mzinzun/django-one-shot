from django.shortcuts import render, redirect, get_object_or_404
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm


# Create your views here.
def todo_list_list(request):
    lists = TodoList.objects.all()
    context = {
        "lists": lists,
    }
    return render(request, "todos/todo_lists.html", context)


def todo_list_detail(request, id):
    details = get_object_or_404(TodoList, id=id)
    context = {
        "details": details,
    }
    return render(request, "todos/details.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            temp = form.save()
            return redirect("todo_list_detail", temp.id)
    else:
        form = TodoListForm()
        context = {"form": form}
        return render(request, "todos/create.html", context)


def todo_list_update(request, id):
    item = TodoList.objects.get(id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=item)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=id)
    else:
        form = TodoListForm(instance=item)
        context = {
            "item": item,
            "form": form,
        }
        return render(request, "todos/update.html", context)


def todo_list_delete(request, id):
    if request.method == "POST":
        item = TodoList.objects.get(id=id)
        item.delete()
        return redirect("todo_list_list")

    return render(request, "todos/delete.html")


def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            temp = form.save()
            return redirect("todo_list_detail", id=temp.list.id)
    else:
        form = TodoItemForm()
        context = {"form": form}
        return render(request, "todos/create_task.html", context)


def todo_item_update(request, id):
    item = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=item)
        if form.is_valid():
            temp = form.save()
            return redirect("todo_list_detail", id=temp.list.id)
    else:
        form = TodoItemForm(instance=item)
        context = {"form": form}
        return render(request, "todos/update_task.html", context)
